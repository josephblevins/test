#include <stdio.h>
#include <stdlib.h>

typedef int (*trand)(void);
typedef int (*tfunc)(trand);

int rand(void) {
    return 8;
}

int qrand(int(*prand)(void)) {
    return (*prand)() * 4;
}

int sqrand(int (*prand1)(void), int (*prand2)(void)) {
    return ((*prand1)() + (*prand2)()) * 4;
}

int drand(trand prand) {
    return (*prand)() * 2;
}

int main(void) {

    int a = drand(&rand);
    int b = qrand(&rand);
    int c = sqrand(&rand, NULL);

    printf("a: %d\nb: %d\nc: %d\n", a, b, c);

    return EXIT_SUCCESS;
}

